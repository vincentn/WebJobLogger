let array = [];
let currentLastJobId="0";
const toolsEnum = Object.freeze({"Edit": 1, "Delete": 2, "Mark": 3});
let currentTool = toolsEnum.Edit;
let colorEnum = Object.freeze({"IntoCRM": 1, "NotIntoCRM": 2,  properties: {
        1: {value: 1, BorderColor: "#f49541",BackgroundColor:"#f4a142"},
        2: {value: 2,  BorderColor: "#3a87ad",BackgroundColor:"#3a87ad"}
    }});
let initColor = colorEnum.NotIntoCRM;

if (typeof(Storage) === "undefined") {
    console.log("Local storage not supported")
}else{
    if(localStorage.getItem("localJobs" )==null){
        console.log("new Browser, no Local jobs found");
        localStorage.setItem("localJobs", JSON.stringify(array));
        localStorage.setItem("LastJobId", "0")
    }else{
        console.log('restore jobs from local storage');
        currentLastJobId = localStorage.getItem("LastJobId")
        array = JSON.parse(localStorage.getItem("localJobs"));
    }
}

$('.toolButton').on('click',function() {
    let string = $(this).attr("data-what");
    console.log("Previous tool: " + currentTool);
    currentTool = toolsEnum[string];
    console.log("New Tool: " + currentTool);
});

function addJob(job){
    job.backgroundColor =colorEnum.properties[initColor].BackgroundColor;
    job.borderColor =colorEnum.properties[initColor].BorderColor;
    job.colorType = initColor;
    ++currentLastJobId;
    job.id= currentLastJobId
    array.push(job);
    console.log(array)

    $('#calendar').fullCalendar( 'renderEvent', job );
    saveCurrentArray()
}
function saveCurrentArray(){
    localStorage.setItem("LastJobId",currentLastJobId);
    localStorage.setItem("localJobs", JSON.stringify(array));
}
function removeFromArrayById(id){
    for(var i = array.length-1; i>=0;i--){
        if(array[i].id === id ){
            array.splice(i,1);
        }
    }
    saveCurrentArray();
}
function updateEvent(event){
    let SimpleEvent =  eventToSimplyfiedObject(event);
    for(var i = array.length-1; i>=0;i--){
        if(array[i].id === SimpleEvent.id ){
            array[i] = SimpleEvent;
        }
    }
    console.log(array);
    saveCurrentArray();
}
function eventToSimplyfiedObject(event){
    return result = {backgroundColor: event.backgroundColor,borderColor: event.borderColor, colorType :  event.colorType,end : event.end,id : event.id,start: event.start, title :event.title}
}

const ondayclick = function (date, jsEvent, view){
    switch (view.name){
        case    "month":
            $('#calendar').fullCalendar('changeView','agendaDay');
            $('#calendar').fullCalendar( 'gotoDate', date );
            break;
        case    "agendaDay":
            var beginDate = date.format();
            var endDate = date.add(1,'h').format();
            addingEvent = {title:'', start  :beginDate ,end : endDate}
            $('#exampleModal').modal('show');
            break;
        case    "listMonth":
         break;
    }
};
let addingEvent = null;
$('#exampleModal').on('shown.bs.modal', function () {
    $('#addJobTitle').focus()
})
$('#exampleModal').on('hidden.bs.modal', function () {
   addingEvent = null;
    $('#addJobTitle').val("");
});
$('#SaveEvent').on('click', function () {
    if(addingEvent != null ){
        if(addingEvent.title != ''){
            addingEvent.title = $('#addJobTitle').val();
            updateEvent(addingEvent)
            $('#calendar').fullCalendar( 'updateEvent', addingEvent );
        }else{
            addingEvent.title = $('#addJobTitle').val();
            addJob(addingEvent);
        }
        $('#addJobTitle').val("");
        addingEvent = null;

    }
    $('#exampleModal').modal('hide');
})
const onEventResizeAndDrop = function( event, delta, revertFunc, jsEvent, ui, view ) {
    updateEvent(event);
}
const onEventClick  = function(calEvent, jsEvent, view) {
    console.log("Event Clicked: "+calEvent.title);
    switch(currentTool){
        case   toolsEnum.Edit:
            console.log("Editing event");
            addingEvent = calEvent;
            $('#addJobTitle').val(calEvent.title)
            $('#exampleModal').modal('show');

            break;
        case   toolsEnum.Delete:
            console.log("Deleting event")
            $('#calendar').fullCalendar('removeEvents',calEvent.id);
            removeFromArrayById(calEvent.id)
            console.log( $('#calendar').fullCalendar( "getEventSources") );
        break;
        case   toolsEnum.Mark:
            console.log("Marking event")
            switch (calEvent.colorType){
                case 1:
                    console.log("change color to not into CRM");
                    calEvent.backgroundColor =colorEnum.properties[colorEnum.NotIntoCRM].BackgroundColor;
                    calEvent.borderColor =colorEnum.properties[colorEnum.NotIntoCRM].BorderColor;
                    calEvent.colorType = colorEnum.NotIntoCRM;
                    break;
                case 2:
                    console.log("change color to into CRM");
                    calEvent.backgroundColor =colorEnum.properties[colorEnum.IntoCRM].BackgroundColor;
                    calEvent.borderColor =colorEnum.properties[colorEnum.IntoCRM].BorderColor;
                    calEvent.colorType = colorEnum.IntoCRM;
                    break;
            }
            $('#calendar').fullCalendar( 'updateEvent', calEvent );
            updateEvent(calEvent);
            break;
    }
}
$(function() {

    // page is now ready, initialize the calendar...

    $('#calendar').fullCalendar({
        height: "auto",

        themeSystem: 'bootstrap4',
        defaultView: 'agendaDay',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaDay,listMonth'
        },
        allDaySlot: false,
        eventClick: onEventClick ,
        weekNumbers: true,
        eventLimit: true, // allow "more" link when too many events
        editable: true,
        eventResize : onEventResizeAndDrop,
        eventDrop: onEventResizeAndDrop,
        dayClick: ondayclick,
        events: fetchLocalyStoredEvents()
    })

});

function fetchLocalyStoredEvents(){
    return array
}